package Examen.S1;

public class A implements Interface1 {

    private final M m;

    public A(B b) { this.m = new M(b, new L()); }

    public void metA() {

        System.out.println("Hello from A");
    }
}
