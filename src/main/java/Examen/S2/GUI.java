package Examen.S2;

import javax.swing.*;

public class GUI extends JFrame {

    private final JTextArea area = new JTextArea();
    private final JTextField textField = new JTextField();
    private final JButton button = new JButton("PRESS");

    public GUI() {

        init();
    }

    private void init() {

        this.setVisible(true);
        this.setSize(500, 500);
        this.setLayout(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        this.area.setBounds(50, 20, 400, 100);
        this.button.setBounds(50, 200, 400, 30);
        this.textField.setBounds(50, 250, 400, 30);

        this.button.addActionListener(e -> {

            String string = textField.getText();

            if(string.equals("")) {

                area.setText("Introduce text");
                return;
            }

            area.setText(string);
        });

        this.add(area);
        this.add(textField);
        this.add(button);
    }
}
